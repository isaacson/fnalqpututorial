import React from "react";
import jsqubits from "jsqubits";
import Layout from "../layout";
import {Bar} from 'react-chartjs-2';
//import Graph from "../components/Plots/randomizedLine";

const qubits = require('jsqubits').jsqubits;

function Hadamard() {
    return qubits('|0>').hadamard(qubits.ALL).measure(qubits.ALL).result;
}

var heads = 0;
var tails = 0;

const initialState = {
    labels: ['|0>','|1>'],
    datasets: [
    {
        label: 'Quantum Coin',
        data: [heads,tails]
    }
    ]
};

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            plot: initialState, 
        };
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        var result = Hadamard();
        if(result == 0) {
            heads++;
        } else {
            tails++;
        }
        var newState = {
            ...initialState,
            datasets: [
            {
                label: 'Quantum Coin',
                data: [heads,tails]
            }
            ]
        };
        this.setState({
            date: new Date(),
            plot: newState,
        });
    }

    render() {
        return(
                <div>
                <h1>Hello, world!</h1>
                <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
                <Bar data={this.state.plot}
                     options={{
                         scales: {
                             yAxes: [{
                                 ticks: {
                                     beginAtZero: true
                                 }
                             }]
                         }
                     }}
                />
                </div>
              );
    }
}

class IndexPage extends React.Component {
    render() {
        return (
            <Layout location={this.props.location}>
            <div>
            <Hadamard />
            <Clock />
            </div>
            </Layout>
            );
    }
}

export default IndexPage;
